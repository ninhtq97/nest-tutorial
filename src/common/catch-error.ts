import { HttpStatus } from '@nestjs/common';
import * as signale from 'signale';
import { throwError } from 'src/utils/app.error';

const catchServerError = (error: { message: string }) => {
  signale.error('Error:', error.message);
  throwError('Oops. Internal Server Error!', HttpStatus.INTERNAL_SERVER_ERROR);
};

export { catchServerError };
