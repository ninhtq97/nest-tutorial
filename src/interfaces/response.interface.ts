export interface ResponseInterface<T> {
  success: boolean;
  reason: number;
  message?: string;
  data?: T;
}
