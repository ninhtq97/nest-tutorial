import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const EnvironmentSchema = new Schema(
  {
    name: { type: String, require: true },
    type: { type: String, require: true },
  },
  { timestamps: true },
);

const EnvironmentModel = mongoose.model('environment', EnvironmentSchema);

export { EnvironmentModel };
