import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
class Environment extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  type: string;
}

const EnvironmentSchema = SchemaFactory.createForClass(Environment);

export { Environment, EnvironmentSchema };
