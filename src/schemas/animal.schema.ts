import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
class Animal extends Document {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  type: string;

  @Prop({
    required: true,
    get: function(eat: string) {
      return eat.split(',');
    },
  })
  eat: string;

  @Prop({ required: true })
  age: number;
}

const AnimalSchema = SchemaFactory.createForClass(Animal);

export { Animal, AnimalSchema };
