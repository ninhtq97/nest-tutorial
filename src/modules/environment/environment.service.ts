import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { catchServerError } from 'src/common/catch-error';
import { Environment } from 'src/schemas/environment.schema';
import { ResponseInterface } from '../../interfaces/response.interface';

@Injectable()
export class EnvironmentService<T> {
  constructor(
    @InjectModel(Environment.name) private envModel: Model<Environment>,
  ) {}

  async getAll(): Promise<ResponseInterface<T[]>> {
    try {
      const environments = await this.envModel.find();

      return {
        success: true,
        reason: 0,
        message: 'Get all environments successfully.',
        data: environments,
      };
    } catch (error) {
      catchServerError(error);
    }
  }

  async getOne(id: string): Promise<ResponseInterface<T>> {
    try {
      const animal = await this.envModel.findById(id);

      return {
        success: true,
        reason: 0,
        message: 'Get environment successfully.',
        data: animal,
      };
    } catch (error) {
      catchServerError(error);
    }
  }

  async addEnvironment(env: T): Promise<ResponseInterface<T>> {
    try {
      const newEnv = await this.envModel.create(env);

      return {
        success: true,
        reason: 0,
        message: 'Add new environment successfully.',
        data: newEnv,
      };
    } catch (error) {
      catchServerError(error);
    }
  }

  async updateEnvironment(
    id: string,
    env: T,
  ): Promise<ResponseInterface<undefined>> {
    try {
      await this.envModel.updateOne({ _id: id }, env);

      return {
        success: true,
        reason: 0,
        message: 'Update environment successfully.',
      };
    } catch (error) {
      catchServerError(error);
    }
  }

  async removeEnvironment(id: string): Promise<ResponseInterface<undefined>> {
    try {
      await this.envModel.deleteOne({ _id: id });

      return {
        success: true,
        reason: 0,
        message: 'Delete environment successfully.',
      };
    } catch (error) {
      catchServerError(error);
    }
  }
}
