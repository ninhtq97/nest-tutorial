import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { Roles } from 'src/decorators/roles.decorator';
import { ResponseInterface } from '../../interfaces/response.interface';
import { createEnvironmentDto, paramsEnvironment } from './dto/environment.dto';
import { EnvironmentService } from './environment.service';
import { EnvironmentInterface } from './interfaces/environment.interface';

@Controller('environment')
export class EnvironmentController {
  constructor(
    private readonly environmentService: EnvironmentService<
      EnvironmentInterface
    >,
  ) {}

  @Get()
  @Roles('admin')
  getAll(): Promise<ResponseInterface<EnvironmentInterface[]>> {
    return this.environmentService.getAll();
  }

  @Get('/:id')
  getOne(
    @Param() { id }: paramsEnvironment,
  ): Promise<ResponseInterface<EnvironmentInterface>> {
    return this.environmentService.getOne(id);
  }

  @Post('/add')
  addEnvironment(
    @Body() environmentDto: createEnvironmentDto,
  ): Promise<ResponseInterface<EnvironmentInterface>> {
    return this.environmentService.addEnvironment(environmentDto);
  }

  @Put('/update/:id')
  updateEnvironment(
    @Param() { id }: paramsEnvironment,
    @Body() environmentDto: createEnvironmentDto,
  ): Promise<ResponseInterface<undefined>> {
    return this.environmentService.updateEnvironment(id, environmentDto);
  }

  @Delete('/remove/:id')
  deleteEnvironment(
    @Param() { id }: paramsEnvironment,
  ): Promise<ResponseInterface<undefined>> {
    return this.environmentService.removeEnvironment(id);
  }
}
