import { IsString } from 'class-validator';

class createEnvironmentDto {
  @IsString()
  name: string;

  @IsString()
  type: string;
}

class paramsEnvironment {
  @IsString()
  id: string;
}

export { createEnvironmentDto, paramsEnvironment };
