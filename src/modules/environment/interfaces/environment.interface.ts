interface EnvironmentInterface {
  name: string;
  type: string;
}

export { EnvironmentInterface };
