import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { ResponseInterface } from '../../interfaces/response.interface';
import { AnimalService } from './animals.service';
import { createAnimalDto, paramsAnimal } from './dto/animal.dto';
import { AnimalInterface } from './interfaces/animal.interface';

@Controller('animal')
export class AnimalController {
  constructor(private readonly animalService: AnimalService<AnimalInterface>) {}

  @Get()
  getAll(): Promise<ResponseInterface<AnimalInterface[]>> {
    return this.animalService.getAll();
  }

  @Get('/:id')
  getOne(
    @Param() { id }: paramsAnimal,
  ): Promise<ResponseInterface<AnimalInterface>> {
    return this.animalService.getOne(id);
  }

  @Post('/add')
  addAnimal(
    @Body() animalDto: createAnimalDto,
  ): Promise<ResponseInterface<AnimalInterface>> {
    return this.animalService.addAnimal(animalDto);
  }

  @Put('/update/:id')
  updateAnimal(
    @Param() { id }: paramsAnimal,
    @Body() animalDto: createAnimalDto,
  ): Promise<ResponseInterface<undefined>> {
    return this.animalService.updateAnimal(id, animalDto);
  }

  @Delete('/remove/:id')
  deleteAnimal(
    @Param() { id }: paramsAnimal,
  ): Promise<ResponseInterface<undefined>> {
    return this.animalService.removeAnimal(id);
  }
}
