import { IsInt, IsString } from 'class-validator';

class createAnimalDto {
  @IsString()
  name: string;

  @IsString()
  type: string;

  @IsString()
  eat: string;

  @IsInt()
  age: number;
}

class paramsAnimal {
  @IsString()
  id: string;
}

export { createAnimalDto, paramsAnimal };
