import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { catchServerError } from 'src/common/catch-error';
import { Animal } from 'src/schemas/animal.schema';
import { ResponseInterface } from '../../interfaces/response.interface';

@Injectable()
export class AnimalService<T> {
  constructor(@InjectModel(Animal.name) private animalModel: Model<Animal>) {}

  async getAll(): Promise<ResponseInterface<T[]>> {
    try {
      const animals = await this.animalModel.find();

      return {
        success: true,
        reason: 0,
        message: 'Get animal successfully.',
        data: animals,
      };
    } catch (error) {
      catchServerError(error);
    }
  }

  async getOne(id: string): Promise<ResponseInterface<T>> {
    try {
      const animal = await this.animalModel.findById(id);

      return {
        success: true,
        reason: 0,
        message: 'Get animal successfully.',
        data: animal,
      };
    } catch (error) {
      catchServerError(error);
    }
  }

  async addAnimal(animal: T): Promise<ResponseInterface<T>> {
    try {
      const newAnimal = await this.animalModel.create(animal);

      return {
        success: true,
        reason: 0,
        message: 'Add new animal successfully.',
        data: newAnimal,
      };
    } catch (error) {
      catchServerError(error);
    }
  }

  async updateAnimal(
    id: string,
    animal: T,
  ): Promise<ResponseInterface<undefined>> {
    try {
      await this.animalModel.updateOne({ _id: id }, animal);

      return {
        success: true,
        reason: 0,
        message: 'Update animal successfully.',
      };
    } catch (error) {
      catchServerError(error);
    }
  }

  async removeAnimal(id: string): Promise<ResponseInterface<undefined>> {
    try {
      await this.animalModel.deleteOne({ _id: id });

      return {
        success: true,
        reason: 0,
        message: 'Delete animal successfully.',
      };
    } catch (error) {
      catchServerError(error);
    }
  }
}
