import { NestFactory, Reflector } from '@nestjs/core';
import * as signale from 'signale';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './filters/http-exception.filter';
import { RolesGuard } from './guards/roles.guard';
import { LoggingInterceptor } from './interceptors/logging.interceptor';
import { TimeoutInterceptor } from './interceptors/timeout.interceptor';
import { ValidationPipe } from './pipes/validation.pipe';

async function startServer() {
  const app = await NestFactory.create(AppModule);

  // const { httpAdapter } = app.get(HttpAdapterHost);
  // app.useGlobalFilters(new AllExceptionsFilter(httpAdapter));
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalGuards(new RolesGuard(new Reflector()));
  app.useGlobalInterceptors(new LoggingInterceptor(), new TimeoutInterceptor());

  // mongoose
  //   .connect(
  //     'mongodb://localhost:27017/dbNest?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false',
  //     {
  //       useNewUrlParser: true,
  //       useUnifiedTopology: true,
  //     },
  //   )
  //   .then(async () => {
  //     signale.success(`MongoDB Connected`);
  //   })
  //   .catch((error: any) => signale.error(`Mongo DB: ${error}`));

  const PORT = process.env.PORT || 5000;
  await app.listen(PORT, () => {
    signale.success(`Server running on http://localhost:${PORT}`);
  });
}

startServer();
