import { HttpException } from '@nestjs/common';

const throwError = (message: string, code: number) => {
  throw new HttpException({ message }, code);
};

export { throwError };
