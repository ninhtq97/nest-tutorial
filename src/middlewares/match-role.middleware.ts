import { UnauthorizedException } from '@nestjs/common';

const matchRoles = (roles: string[], role: string) => {
  if (roles.includes(role)) {
    return true;
  } else {
    throw new UnauthorizedException();
  }
};

export { matchRoles };
