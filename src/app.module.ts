import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { logger } from './middlewares/logger.middleware';
import { AnimalModule } from './modules/animals/animal.module';
import { AnimalController } from './modules/animals/animals.controller';
import { AuthModule } from './modules/auth/auth.module';
import { EnvironmentController } from './modules/environment/environment.controller';
import { EnvironmentModule } from './modules/environment/environment.module';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    AnimalModule,
    EnvironmentModule,
    MongooseModule.forRoot(
      'mongodb://localhost:27017/dbNest?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false',
    ),
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(logger).forRoutes(AnimalController, EnvironmentController);
  }
}
